#ifndef __eloquent__DatagramReader__
#define __eloquent__DatagramReader__

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C
#include <syslog.h>

// C++
#include <iostream>

// Boost
#include <boost/property_tree/ptree.hpp>
#include <boost/asio.hpp>

// Internal
#include "Eloquent/Extensions/IO/IO.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// DatagramWriter : IOExtension
	///////////////////////////////////////////////////////////////////////////////
	class DatagramReader : public IO {
		DatagramReader();
		
	public:
		explicit DatagramReader( const boost::property_tree::ptree::value_type& i_Config
								, std::mutex& i_QueueMutex
								, std::condition_variable& i_QueueCV
								, std::queue<QueueItem>& i_Queue
								, unsigned int& i_NumWriters );
		
		virtual ~DatagramReader();
		virtual void operator()();
		
	private:
		// Networking
		boost::asio::io_service* m_IOService;
		boost::asio::ip::udp::socket* m_Socket; 
		
	};
}
#endif /* defined(__eloquent__DatagramReader__) */
