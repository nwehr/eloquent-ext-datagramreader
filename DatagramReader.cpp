//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C++
#include <iostream>
#include <string>
#include <cstring>

// Internal
#include "DatagramReader.h"

///////////////////////////////////////////////////////////////////////////////
// DatagramReader : IOExtension
///////////////////////////////////////////////////////////////////////////////
Eloquent::DatagramReader::DatagramReader( const boost::property_tree::ptree::value_type& i_Config
										 , std::mutex& i_QueueMutex
										 , std::condition_variable& i_QueueCV
										 , std::queue<QueueItem>& i_Queue
										 , unsigned int& i_NumWriters)
: IO( i_Config, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters )
, m_IOService( new boost::asio::io_service() )
, m_Socket( new boost::asio::ip::udp::socket( *m_IOService ) )
{
	m_Socket->open( boost::asio::ip::udp::v4() );
	
	unsigned short Port = m_Config.second.get<unsigned short>( "listen" );
	boost::optional<std::string> Bind = m_Config.second.get_optional<std::string>( "bind" );
	
	boost::asio::ip::udp::endpoint local_endpoint;
	
	local_endpoint.port( Port );
	
	if( Bind.is_initialized() ) {
		local_endpoint.address( boost::asio::ip::address::from_string( Bind.get() ) );
	}
	
	m_Socket->bind( local_endpoint );
	
//	syslog( LOG_INFO, "setting up a reader for port %d #Comment #Reader #DatagramReader", m_Config.second.get<int>( "listen" ) );
	
}

Eloquent::DatagramReader::~DatagramReader() {
//	syslog( LOG_INFO, "shutting down a reader for port %d #Comment #Reader #DatagramReader", m_Config.second.get<int>( "listen" ) );
	
	delete m_Socket;
	delete m_IOService;

}

void Eloquent::DatagramReader::operator()() {
	while( true ) {
		try {
			char Data[4096] = { "" };
			
			boost::asio::ip::udp::endpoint remote_endpoint;
			boost::system::error_code error;
			
			m_Socket->receive_from( boost::asio::buffer( Data, 4096 ), remote_endpoint, 0, error );
			
			if( error && error != boost::asio::error::message_size )
				throw error;
			
			PushQueueItem( QueueItem( Data, (m_SetOrigin.is_initialized() ? *m_SetOrigin : remote_endpoint.address().to_string()) ) );

		} catch( std::exception& e ) {
			syslog( LOG_ERR, "%s #Error #Reader #DatagramReader", e.what() );
		}
		
	}
	
}